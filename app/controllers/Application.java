package controllers;

import models.ChatMessage;
import play.*;
import play.mvc.*;

import views.html.*;

import java.util.ArrayList;

public class Application extends Controller {

    public static ArrayList<ChatMessage> messages = new ArrayList<>();

    public static Result postMessage(String user, String msg){
        messages.add(0, new ChatMessage(user, msg));
        return ok("Added Message");
    }

    public static Result index(){
        return ok(index.render(messages));
    }

    public static Result getMessages(){
        return ok(chatMessages.render(messages));
    }
}
