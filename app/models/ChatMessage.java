package models;

public class ChatMessage {
    public final String user;
    public final String msg;

    public ChatMessage(String user, String msg){
        this.user = user;
        this.msg = msg;
    }
}
