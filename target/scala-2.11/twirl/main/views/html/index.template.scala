
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[java.util.ArrayList[ChatMessage],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(messages: java.util.ArrayList[ChatMessage]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.46*/("""

"""),_display_(/*3.2*/main("Chat-Beispiel")/*3.23*/ {_display_(Seq[Any](format.raw/*3.25*/("""
    """),format.raw/*4.5*/("""<div class="col-md-4 col-md-offset-4 buffer">
        <div class="input-group">
            <input type="text" class="form-control" id="messageTf" placeholder="Enter your message">
            <span class="input-group-btn">
                <button class="btn btn-success" id="sendBtn" type="button">Send!</button>
            </span>
        </div>
    </div>
    <div id="chatHistory">
        """),_display_(/*13.10*/chatMessages(messages)),format.raw/*13.32*/("""
    """),format.raw/*14.5*/("""</div>
""")))}),format.raw/*15.2*/("""
"""))}
  }

  def render(messages:java.util.ArrayList[ChatMessage]): play.twirl.api.HtmlFormat.Appendable = apply(messages)

  def f:((java.util.ArrayList[ChatMessage]) => play.twirl.api.HtmlFormat.Appendable) = (messages) => apply(messages)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Apr 24 12:39:12 CEST 2015
                  SOURCE: /Users/moritzhofner/Desktop/sample_chat/app/views/index.scala.html
                  HASH: 3b064b1543c3fc338be6765ed03e49a37d68f73f
                  MATRIX: 749->1|881->45|909->48|938->69|977->71|1008->76|1431->472|1474->494|1506->499|1544->507
                  LINES: 26->1|29->1|31->3|31->3|31->3|32->4|41->13|41->13|42->14|43->15
                  -- GENERATED --
              */
          