
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.32*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>

<html>
    <head>
        <title>"""),_display_(/*7.17*/title),format.raw/*7.22*/("""</title>
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <script src=""""),_display_(/*12.23*/routes/*12.29*/.Assets.at("javascripts/chat.js")),format.raw/*12.62*/(""""></script>
        <link href=""""),_display_(/*13.22*/routes/*13.28*/.Assets.at("stylesheets/main.css")),format.raw/*13.62*/("""" rel="stylesheet">
    </head>
    <body>
        """),_display_(/*16.10*/content),format.raw/*16.17*/("""
    """),format.raw/*17.5*/("""</body>
</html>
"""))}
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Apr 24 12:40:14 CEST 2015
                  SOURCE: /Users/moritzhofner/Desktop/sample_chat/app/views/main.scala.html
                  HASH: d1d4bb756e9c4fddbec24e35dd55191777f2d726
                  MATRIX: 727->1|845->31|873->33|950->84|975->89|1307->394|1322->400|1376->433|1436->466|1451->472|1506->506|1585->558|1613->565|1645->570
                  LINES: 26->1|29->1|31->3|35->7|35->7|40->12|40->12|40->12|41->13|41->13|41->13|44->16|44->16|45->17
                  -- GENERATED --
              */
          