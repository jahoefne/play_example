
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object chatMessages extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[java.util.ArrayList[ChatMessage],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(messages: java.util.ArrayList[ChatMessage]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.46*/("""
"""),format.raw/*2.1*/("""<div class="col-md-4 col-md-offset-4">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Chat-Nachrichten</h3>
        </div>
        <table class="table table-striped">
            """),_display_(/*8.14*/for(msg <- messages) yield /*8.34*/ {_display_(Seq[Any](format.raw/*8.36*/("""
                """),format.raw/*9.17*/("""<tr>
                    <td> """),_display_(/*10.27*/msg/*10.30*/.user),format.raw/*10.35*/(""":</td>
                    <td> """),_display_(/*11.27*/msg/*11.30*/.msg),format.raw/*11.34*/("""</td>
                </tr>
            """)))}),format.raw/*13.14*/("""
        """),format.raw/*14.9*/("""</table>
    </div>
</div>"""))}
  }

  def render(messages:java.util.ArrayList[ChatMessage]): play.twirl.api.HtmlFormat.Appendable = apply(messages)

  def f:((java.util.ArrayList[ChatMessage]) => play.twirl.api.HtmlFormat.Appendable) = (messages) => apply(messages)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Apr 24 12:46:12 CEST 2015
                  SOURCE: /Users/moritzhofner/Desktop/sample_chat/app/views/chatMessages.scala.html
                  HASH: 2846b87a7b819818dcf5143a946ef56c0d0398eb
                  MATRIX: 756->1|888->45|915->46|1184->289|1219->309|1258->311|1302->328|1360->359|1372->362|1398->367|1458->400|1470->403|1495->407|1567->448|1603->457
                  LINES: 26->1|29->1|30->2|36->8|36->8|36->8|37->9|38->10|38->10|38->10|39->11|39->11|39->11|41->13|42->14
                  -- GENERATED --
              */
          