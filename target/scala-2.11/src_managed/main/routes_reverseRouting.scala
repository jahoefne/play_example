// @SOURCE:/Users/moritzhofner/Desktop/sample_chat/conf/routes
// @HASH:be83ab2a789c607e59c4be56ece340b9cad2c7d1
// @DATE:Fri Apr 24 12:33:49 CEST 2015

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString


// @LINE:15
// @LINE:10
// @LINE:8
// @LINE:6
package controllers {

// @LINE:15
class ReverseAssets {


// @LINE:15
def at(file:String): Call = {
   implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                        

}
                          

// @LINE:10
// @LINE:8
// @LINE:6
class ReverseApplication {


// @LINE:8
def postMessage(user:String, msg:String): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "messages" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("user", user)), Some(implicitly[QueryStringBindable[String]].unbind("msg", msg)))))
}
                        

// @LINE:6
def index(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix)
}
                        

// @LINE:10
def getMessages(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "messages")
}
                        

}
                          
}
                  


// @LINE:15
// @LINE:10
// @LINE:8
// @LINE:6
package controllers.javascript {
import ReverseRouteContext.empty

// @LINE:15
class ReverseAssets {


// @LINE:15
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        

}
              

// @LINE:10
// @LINE:8
// @LINE:6
class ReverseApplication {


// @LINE:8
def postMessage : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.postMessage",
   """
      function(user,msg) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "messages" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("user", user), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("msg", msg)])})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:10
def getMessages : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.getMessages",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "messages"})
      }
   """
)
                        

}
              
}
        


// @LINE:15
// @LINE:10
// @LINE:8
// @LINE:6
package controllers.ref {


// @LINE:15
class ReverseAssets {


// @LINE:15
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      

}
                          

// @LINE:10
// @LINE:8
// @LINE:6
class ReverseApplication {


// @LINE:8
def postMessage(user:String, msg:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.postMessage(user, msg), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "postMessage", Seq(classOf[String], classOf[String]), "POST", """""", _prefix + """messages""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "index", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:10
def getMessages(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.getMessages(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "getMessages", Seq(), "GET", """""", _prefix + """messages""")
)
                      

}
                          
}
        
    