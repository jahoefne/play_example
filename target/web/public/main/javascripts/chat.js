$(document).ready(function () {

    function updateChatHistory() {
        $.get("/messages", function (data) {
            $("#chatHistory").html(data);
        });
        setTimeout(updateChatHistory, 1000);
    }

    function sendMessage() {
        $.post("/messages?msg=" + messageTf.val() + "&user=" + user);
        messageTf.val("");
    }

    var messageTf = $("#messageTf");
    var sendBtn = $("#senBtn");
    var user = prompt("Please enter your name");

    sendBtn.click(function () {
        sendMessage()
    });

    messageTf.keypress(function (e) {
        if (e.which == 13)
            sendMessage()
    });

    setTimeout(updateChatHistory, 1000);
});